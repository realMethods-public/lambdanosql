# Specify the provider and access details 
provider "aws" {
  region     = "us-east-1"
  access_key = "${var.aws-access-key}"
  secret_key = "${var.aws-secret-key}"
}



# Our default security group to for the database
resource "aws_security_group" "mongo" {
  description = "security group created from terraform"
  vpc_id      = "vpc-c422e2a0"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  # mongodb access from anywhere
  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "mongodb" {
  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    # The default username for our AMI
    user = "ubuntu"
  }

  instance_type = "t2.micro"
  
  tags { Name = "mongodb instance" } 

  # standard realmethods community image with mongo started on the default port 
  ami = "ami-0e2a167cf2e0ce6c0"

  # The name of the SSH keypair you've created and downloaded
  # from the AWS console.
  #
  # https://console.aws.amazon.com/ec2/v2/home?region=us-west-2#KeyPairs:
  #
  key_name = "my-public-key"
  
  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.mongo.id}"]
  
  # To ensure ssh access works
    provisioner "remote-exec" {
    inline = [
      "sudo ls",
    ]
  }
}

 resource "aws_lambda_function" "getAlleyLeagues" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAlleyLeagues"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::getLeagues"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
}

